const express = require('express');
const router = express.Router();
//var CronJob = require('cron').CronJob;

const onlineposController = require('../controllers/onlineposController');

// Server index page
router.get("/", function (req, res) {

  res.render('frontpage', {
    title: 'OnlinePOS/Plecto syncronizer is running...',
  });
});


router.get('/export-sales', onlineposController.viewExportSales );
router.get('/import-sales', onlineposController.importSales );


router.get('/view-orders', onlineposController.viewOrders );
router.get('/import-orders', onlineposController.importOrders );

module.exports = router;