const onlineposController = require('./controllers/onlineposController');

function runScheduledJob() {
	onlineposController.importSales();
	onlineposController.importOrders();

	console.log('Imports executed');
}
runScheduledJob();

