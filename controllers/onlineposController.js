const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const moment = require('moment');
const request = require('request');
const requestp = require('request-promise-native');

// Better errors
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});


/**
 * Get sales from OnlinePOS
 * @return {[type]} [description]
 */
function getExportSales() {
	console.log('getting exportsSales');
	var base_url = process.env.ONLINE_POS_BASE_URL;
	var customer_id = process.env.ONLINE_POS_CUSTOMER_ID;
	var unique_token = process.env.ONLINE_POS_UNIQUE_TOKEN;

	// Empty both moments datestring, when we're live!!!!
	// var now_unix = moment('2018-04-13').unix();
	// var two_days_ago_unix = moment('2018-04-12').subtract(4, 'days').unix();
	
	var now_unix = moment().unix();
	var two_days_ago_unix = moment().subtract(2, 'days').unix();

	/**
	 * Get all orders
	 * @type {Object}
	 */
	var options = {
		method: 'post',
		//uri: 'https://api.onlinepos.dk/api/getProducts',
		uri: 'https://api.onlinepos.dk/api/exportSales',
		//uri: 'https://api.onlinepos.dk/api/getByUnixTimeSales',
		headers: {
			'accept': 'application/json',
			'content-type': 'application/json',
			'token': unique_token,
			'firmaid': customer_id,
		},
		formData: {
			'from': two_days_ago_unix,
			'to': now_unix,
			'json': 'true',
		},
	}

	// Return promise
	return requestp(options);
}

/**
 * Get sales from OnlinePOS
 * @return {[type]} [description]
 */
function getProducts() {
	console.log('getting products');
	var base_url = process.env.ONLINE_POS_BASE_URL;
	var customer_id = process.env.ONLINE_POS_CUSTOMER_ID;
	var unique_token = process.env.ONLINE_POS_UNIQUE_TOKEN;

	/**
	 * Get all orders
	 * @type {Object}
	 */
	var options = {
		method: 'get',
		//uri: 'https://api.onlinepos.dk/api/getProducts',
		uri: 'https://api.onlinepos.dk/api/getProducts?json="1',
		//uri: 'https://api.onlinepos.dk/api/getByUnixTimeSales',
		headers: {
			'accept': 'application/json',
			'content-type': 'application/json',
			'token': unique_token,
			'firmaid': customer_id,
		},
	}

	// Return promise
	return requestp(options);
}

/**
 * View list of sales
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
exports.viewExportSales = async (req, res) => {

	console.log('Viewing Sales');
	var sales = [];

	var products = [];

	await getProducts()
		.then(function(response) {
			console.log('pros:');
			response = JSON.parse(response);
			products = response.products;

			return getExportSales();
		})
		.then(function(response){
			response = JSON.parse(response);
			//console.log(response.sales);
			sales = response.sales;

			// Add production time to lines
			sales = mapProductTimeToSales(sales);
			
			// Add product cost price
			sales = mapProductInfoToSales(sales, products);
		})
		.catch(function(err){
			console.error(err);
		});

	res.render('exportsales', {
	  title: 'exportSales / line items (last 4 days)',
	  sales: sales
	});

}

/**
 * Import Sales to plecto
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
exports.importSales = async () => {

	var sales;
	var products;
	var sales_amount;


	await getProducts()
		.then(function(response) {
			console.log('pros:');
			response = JSON.parse(response);
			products = response.products;

			return getExportSales();
		})
		// Then parse the order data and save to Plecto 
		.then(function(response) {
			response = JSON.parse(response);
			sales = response.sales;

			sales_amount = sales.length;

			// Add production time to lines
			sales = mapProductTimeToSales(sales);
			
			// Add product cost price
			sales = mapProductInfoToSales(sales, products);

			// Save to plecto
			return saveSales(sales);
		})
		.then(function(response) {
			console.log('response from plecto:');
			console.log(response);
			console.log(sales_amount + ' sales saved to plecto successfully');
		})
		.catch(function(err) {
			console.error(err);
		})
}


/**
 * Save line items to Plecto
 * @return {[type]} [description]
 */
function saveSales(sales) {
	
	//console.log(sales);
	var plecto_base_url = process.env.PLECTO_BASE_URL;
	var api_key = process.env.PLECTO_API_KEY;
	var customer_id = process.env.ONLINE_POS_CUSTOMER_ID;

	// Map Plecto specific data to each order
	var sales = sales.map(function(sale) {
	  
		// Format sales date
		var sale_date = moment(sale.line.date + ' ' + sale.line.time, "DD.MM.YYYY HH:mm");
		
		//console.log(sales_date.format()); // "2014-09-08T08:02:17-05:00" (ISO 8601, no fractional seconds)
		//console.log(sales_date.format('YYYY-MM-DD HH:mm'));

		sale = {
			'data_source': process.env.PLECTO_DATA_SOURCE_SALES,
			'date': sale_date.format(),
			'member_api_provider': process.env.PLECTO_MEMBER_API_PROVIDER,
			'member_api_id': process.env.PLECTO_MEMBER_API_ID,
			'member_name': process.env.PLECTO_MEMBER_NAME,
			'external_id': sale.line.id,

			'id': sale.line.id,
			'chk': sale.line.chk,
			'SalesDate': sale_date.format(),
			'ItemNo': sale.line.product_id,
			'ItemText': sale.line.product,
			'ItemGroupID': sale.line.product_group_id,
			'ItemGroupText': sale.line.product_group,
			'Qty': sale.line.amount,
			'TotalInclVAT': sale.line.price.toString().replace(",", "."),
			'TotalExclVAT': sale.line.price.toString().replace(",", ".") * 0.8,
			'UnitPrice': sale.line.price.toString().replace(",", ".") / sale.line.amount,
			'UnitPriceExclVAT': (sale.line.price.toString().replace(",", ".") / sale.line.amount) * 0.8,
			'Cost': sale.line.cost_price,
			'CompanyID': customer_id,
			'SalesType': sale.line.payment_type,
			'Department': 'Broen',
			'average_time_in_seconds': sale.line.average_time_in_seconds,
		}
	  return sale;
	})

	var plecto_options;
	var promise_array = [];
	var i;
	var j;
	var sale_chunk;
	var chunk = 100; // add 100 registrations per request.
	
	for (i=0, j=sales.length; i<j; i+=chunk) {
		sale_chunk = sales.slice(i,i+chunk);
		
		plecto_options = {
		   method: 'post',
		   uri: plecto_base_url,
		   headers: {
			   'Accept': 'application/json',
			   'Content-Type': 'application/json',
			   'X-integration': process.env.PLECTO_X_INTEGRATION,
			   'X-Company': process.env.PLECTO_X_COMPANY
		   },
		   body: sale_chunk,
		   auth: {
			   user: process.env.PLECTO_USER_EMAIL,
			   password: process.env.PLECTO_USER_PASSWORD
		   },
		   json: true
		}

		//console.log(order_chunk.length);
		promise_array.push( requestp(plecto_options) );  
	}

	//console.log(promise_array);
	return Promise.all(promise_array);
}


function formatExportSalesAsOrderTotals(sales) {

	var orders = sales.reduce(function(groups, item){
		
		//Convert price to number
		item.line.price = item.line.price.toString().replace(",", ".");
		item.line.price = Number(item.line.price);

		// Setup order data
		var order = {
			id: item.line.chk,
			chk: item.line.chk,
			date: item.line.date,
			time: item.line.time,
			price: item.line.price,
			department: 'Broens',
			terminal: item.line.department,
			payment_type: item.line.payment_type,
		}

		
		// Returns index or -1 if it doesnt exist.
		const groupIndex = groups.findIndex(order => order.chk === item.line.chk);
		
		// if it exists, sum price
		if (groupIndex !== -1) {
			groups[groupIndex].price = groups[groupIndex].price + item.line.price;
		} else {
			// Else add it as a new order
			groups.push(order);
		};
		return groups;

	}, []);

	return orders;
}

function mapProductTimeToSales(sales) {

	product_times = [
		{
			ID: 2089874,
			name: 'Havregrød 1',
			average_time_in_seconds: 50,
		},
		{
			ID: 2089875,
			name: 'Chia special',
			average_time_in_seconds: 53,
		},
		{
			ID: 2089876,
			name: 'Daal',
			average_time_in_seconds: 38,
		},
		{
			ID: 2089877,
			name: 'Congee',
			average_time_in_seconds: 31,
		},
		{
			ID: 2095352,
			name: 'Rabarber Lemonade',
			average_time_in_seconds: 12,
		},
		{
			ID: 2095353,
			name: 'Citron Ingefær Lemonade',
			average_time_in_seconds: 12,
		}

	];

	var mapped_sales = sales.map((val, index, arr) => {

		var product_id = val.line.product_id;
		//console.log(product_id);
		
		// Returns index or -1 if it doesnt exist.
		var product = product_times.findIndex(product => product.ID === product_id);
		//console.log(product);

		// if it exists, sum price
		if (product !== -1) {
			val.line.average_time_in_seconds = product_times[product].average_time_in_seconds;
		} else {
			// Else add it as a new order
			val.line.average_time_in_seconds = 0;
		};

		return val;		

	});

	return mapped_sales;

}

/**
 * Add product info (cost price) to line items. 
 * @param  {[type]} sales    [description]
 * @param  {[type]} products [description]
 * @return {[type]}          [description]
 */
function mapProductInfoToSales(sales, products) {

	var mapped_sales = sales.map((val, index, arr) => {

		var product_id = val.line.product_id;
		//console.log(product_id);
		
		// Returns index or -1 if it doesnt exist.
		var product = products.findIndex(product => product.ID === product_id);
		//console.log(product);

		// if it exists, sum price
		if (product !== -1) {
			val.line.cost_price = Number(products[product].cost_price);
		} else {
			// Else add it as a new order
			val.line.cost_price = 0;
		};

		return val;
	});

	return mapped_sales;

}



/**
 * View orders Based on exportSales
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
exports.viewOrders = async (req, res) => {
	console.log('Viewing Sales');
	var sales;
	var orders;

	await getExportSales()
		.then(function(response){
			response = JSON.parse(response);
			//console.log(response.sales);
			sales = response.sales;

			orders = formatExportSalesAsOrderTotals(sales);
		})
		.catch(function(err){
			console.error(err);
		});

	res.render('orders', {
	  title: 'Orders constructed from exportSales / line items (last 4 days)',
	  orders: orders
	});

}

/**
 * Import Sales to plecto
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
exports.importOrders = async () => {

	var orders;
	var orders_amount;


	await getExportSales()
		// Then parse the order data and save to Plecto 
		.then(function(response) {
			response = JSON.parse(response);
			sales = response.sales;

			orders = formatExportSalesAsOrderTotals(sales);

			orders_amount = orders.length;

			// Save to plecto
			return saveOrders(orders);
		})
		.then(function(response) {
			console.log('response from plecto:');
			console.log(response);
			console.log(orders_amount + ' orders saved to plecto successfully');
		})
		.catch(function(err) {
			console.error(err);
		})
}


/**
 * Save Orders to Plecto
 * @return {[type]} [description]
 */
function saveOrders(orders) {
	
	//console.log(sales);
	var plecto_base_url = process.env.PLECTO_BASE_URL;
	var api_key = process.env.PLECTO_API_KEY;
	var customer_id = process.env.ONLINE_POS_CUSTOMER_ID;

	// Map Plecto specific data to each order
	var orders = orders.map(function(order) {
	  
		// Format sales date
		var order_date = moment(order.date + ' ' + order.time, "DD.MM.YYYY HH:mm");
		
		//console.log(sales_date.format()); // "2014-09-08T08:02:17-05:00" (ISO 8601, no fractional seconds)
		//console.log(sales_date.format('YYYY-MM-DD HH:mm'));

		sale = {
			'data_source': process.env.PLECTO_DATA_SOURCE_ORDERS,
			'date': order_date.format(),
			'member_api_provider': process.env.PLECTO_MEMBER_API_PROVIDER,
			'member_api_id': process.env.PLECTO_MEMBER_API_ID,
			'member_name': process.env.PLECTO_MEMBER_NAME,
			'external_id': order.id,

			'id': order.id,
			'chk': order.chk,
			'order_date': order_date.format(),
			'TotalInclVAT': order.price,
			'TotalExclVAT': order.price * 0.8,
			'CompanyID': customer_id,
			'payment_type': order.payment_type,
			'department': order.department,
		}
	  return sale;
	})

	var plecto_options;
	var promise_array = [];
	var i;
	var j;
	var order_chunk;
	var chunk = 100; // add 100 registrations per request.
	
	for (i=0, j=orders.length; i<j; i+=chunk) {
		order_chunk = orders.slice(i,i+chunk);
		
		plecto_options = {
		   method: 'post',
		   uri: plecto_base_url,
		   headers: {
			   'Accept': 'application/json',
			   'Content-Type': 'application/json',
			   'X-integration': process.env.PLECTO_X_INTEGRATION,
			   'X-Company': process.env.PLECTO_X_COMPANY
		   },
		   body: order_chunk,
		   auth: {
			   user: process.env.PLECTO_USER_EMAIL,
			   password: process.env.PLECTO_USER_PASSWORD
		   },
		   json: true
		}

		//console.log(order_chunk.length);
		promise_array.push( requestp(plecto_options) );  
	}

	//console.log(promise_array);
	return Promise.all(promise_array);
}



// function constructOrdersFromSales(sales) {

// 	var orders = sales[2];

// 	// for (i=0, j=sales.length; i<j; i++) {	

// 	// 	var order = {
// 	// 		chk: sale.line.chk,
// 	// 	}
	
// 	// 	orders.push(order);	
// 	// }
// 	console.log(orders);
// }
