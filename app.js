const express = require('express');
const bodyParser = require("body-parser");
const routes = require('./routes/index');
const path = require('path');
const CronJob = require('cron').CronJob;

// import environmental variables from our variables.env file
require('dotenv').config({ path: 'variables.env' });

const app = express();

const onlineposController = require('./controllers/onlineposController');

// view engine setup
app.set('views', path.join(__dirname, 'views')); // this is the folder where we keep our pug files
app.set('view engine', 'pug'); // we use the engine pug, mustache or EJS work great too

// serves up static files from the public folder. Anything in public/ will just be served up as the file it is
app.use(express.static(path.join(__dirname, 'public')));

// A middleware to parse incoming request bodies.
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.listen((process.env.PORT || 5080));

// After allllll that above middleware, we finally handle our own routes!
app.use('/', routes);

// done! we export it so we can start the site in start.js
module.exports = app;