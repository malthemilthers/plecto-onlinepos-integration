# Online POS / Plecto integration
Integration app sending data from OnlinePos to Plecto.

OnlinePOS API: http://api.onlinepos.dk

## PLEASE READ THIS
This application was not build with the intention of sharing, but was simply build to solve a real business problem. So do not consider it a finished product or app. It is definitely not a plug-and-play solution. But hopefully the code can be used to learn from. 

This application was build for a Heroku server with a Heroku Scheduler that runs the scheduler.js script. This is an important step. Please read below.


# Notes on Heroku environment
Heroku is not really great with CronJobs. Instead we use a heroku Scheduler that runs every 10 minutes. 
Also note that the emails don't seem to work on heroku with the Gmail Login. So this has been disabled for now.
Also note that because Heroku is a horizontal server system, we can't relie on the file-system to save our syncKey. Instead we're could experiment with Redis as an in-memory key:value store.


## Notes


### exportSales Mapping
### External ID & ID
id: 251009515,

### chk
chk: 142,

### Date (Combine date and time to YYYY-M-D h:i)
date: '11.04.2018', 
time: '12:47',

### SalesDate (YYYY-M-D 02:00:00)
date: '11.04.2018', 

### ItemNo
product_id: 844038,

### ItemText (tjek op på om det er korrekt tastet in i POS'en)
product: 'Kaffe menu',

### ItemGroupId
product_group_id: 69225,

### ItemGroupText (tjek op på om det er korrekt tastet in i POS'en)
product_group: 'Diningweek',

### Qty (figure out if this is problematic)
amount: 2,

### TotalInclVAT (tjek op på at det er inklusiv moms)
price: '130,00',

### TotalExclVAT (tjek op på at det er inklusiv moms)
price: '130,00', (* 0.8)

### UnitPrice (same as price — maybe not important)
price: '130,00', (* 0.8)

### Cost
MISSING!!!

### Company ID (hardcoded)
11178

### SalesType
payment_type: 'Cash',

### Department (hardcoded)
Broens Gadekoekken
department: 'MM-terminal',

### Not needed: 
clerk: '0',
pax: 0,
ean_1: '',
ean_2: '',
ean_3: '',
ean_4: ''


### /getByUnixTimeSales mapping

external_id: chk
id; chk
date: 
TotalInclVAT: Sum of sale.line.price with same chk
TotalExclVAT: Sum of sale.line.price with same chk * 0.8
CompanyID: 11178
Department: 'Broen'




location
UserId
Amount
pax


## Changelog

### 1.0.0
* First working integration